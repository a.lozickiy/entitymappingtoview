import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {FlatTreeControl} from "@angular/cdk/tree";
import {MatTreeFlatDataSource, MatTreeFlattener} from "@angular/material/tree";
import {FormControl, Validators} from "@angular/forms";
import {debounceTime} from "rxjs";
import {Airport, AllowedDirection, City} from "../../../mock.model";
import {IAirportSelector} from "../../../@core/order/order.interface";

interface CountryNode {
  name: string;
  id: number;
  systemName: string;
  children?: CountryNode[];
}

interface AirportFlatNode {
  expandable: boolean;
  level: number;
  id: number;
  name: string;
  cityId: number;
  iata: string;
  parentId?: number;
  isVirtual: boolean;
  icao: string;
  altitude?: number;
  latitude?: number;
  longitude?: number;
  timezone?: number;
  systemName: string;
  isDeleted: boolean;
}

@Component({
  selector: 'app-airport-selector',
  templateUrl: './airport-selector.component.html',
  styleUrls: ['./airport-selector.component.scss'],
})
export class AirportSelectorComponent implements OnChanges {
  @Input() public airportSelectorData: IAirportSelector[] = [];
  @Input() public allowedDirections: AllowedDirection[] = [];
  @Input() public airportDirection!: Airport;
  @Input() public airport: Airport | null = null;
  @Input() public placeholder: string = '';
  @Input() public flag: string = '';
  @Output() public changeAirport: EventEmitter<Airport | null> = new EventEmitter<Airport | null>();

  public allowedAirports: IAirportSelector[] = [];
  public countryControl = new FormControl<string>('', {nonNullable: true, validators: [Validators.required]});

  private _transformer = (node: any, level: number) => {
    return {
      expandable: !!node.children && node.children.length > 0,
      level: level,
      name: node.name,
      id: node.id,
      cityId: node.cityId,
      iata: node.iata,
      parentId: node.parentId,
      isVirtual: node.isVirtual,
      icao: node.icao,
      altitude: node.altitude,
      latitude: node.latitude,
      longitude: node.longitude,
      timezone: node.timezone,
      systemName: node.systemName,
      isDeleted: node.isDeleted
    };
  };

  treeControl = new FlatTreeControl<any>(
    node => node.level,
    node => node.expandable,
  );

  treeFlattener = new MatTreeFlattener(
    this._transformer,
    node => node.level,
    node => node.expandable,
    node => node.children,
  );

  dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

  constructor() {
    this.subscribeEmitters();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.initTreeData();
  }

  hasChild = (_: number, node: AirportFlatNode) => node.expandable;

  filterRecursive(filterText: string, array: IAirportSelector[], property: string) {
    let filteredData;

    function copy(o: any) {
      return Object.assign({}, o);
    }

    if (filterText) {
      filterText = filterText.toLowerCase();
      filteredData = array.map(copy).filter(function x(y) {
        if (y[property].toLowerCase().includes(filterText)) {
          return true;
        }
        if (y.children) {
          return (y.children = y.children.map(copy).filter(x)).length;
        }
      });
    } else {
      filteredData = array;
    }

    return filteredData;
  }

  filterTree(filterText: string) {
    this.dataSource.data = this.filterRecursive(filterText, this.allowedAirports, 'name');
    this.treeControl.expandAll();
  }

  public initTreeData(): void {
    if (this.airport) {
      this.countryControl.setValue(this.airport.name + ' (' + this.airport.icao + ')');
    }

    const allowedCitiesIdToFly = this.allowedDirections.reduce((acc: number[], item: AllowedDirection) => {
      return this.flag === 'from'
        ? (item['cityToId'] === this.airportDirection?.cityId ? [...acc, item['cityFromId']] : acc)
        : (item['cityFromId'] === this.airportDirection?.cityId ? [...acc, item['cityToId']] : acc)
    }, []);

    if (this.airportDirection?.cityId) {
      this.allowedAirports = this.airportSelectorData.reduce((acc: any[], country: any) => {
        const cities = country.children.filter((city: City) => allowedCitiesIdToFly.includes(city.id));
        return cities.length > 0 ? [...acc, {...country, children: cities}] : acc
      }, [])
    } else {
      this.allowedAirports = this.airportSelectorData;
    }
    console.log(this.allowedAirports);

    this.dataSource.data = this.allowedAirports;
  }

  public emitAirport(airportNode: AirportFlatNode): void {
    const {expandable, level, ...airport} = airportNode;
    this.changeAirport.emit(airport);
  }

  public reset(): void {
    this.countryControl.reset();
  }

  private subscribeEmitters(): void {
    this.countryControl.valueChanges
      .pipe(
        debounceTime(300),
      ).subscribe((airport: string) => {
        this.filterTree(airport);
        if (airport === '') {
          this.changeAirport.emit(null);
          this.treeControl.collapseAll();
        }
      }
    )
  }
}

