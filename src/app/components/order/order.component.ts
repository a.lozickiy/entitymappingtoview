import {Component, OnInit} from '@angular/core';
import {FormArray, FormGroup, NonNullableFormBuilder, Validators} from "@angular/forms";
import {Airport, AllowedDirection, Nationality, Order, PassengerDocumentType} from "../../mock.model";
import {OrderService} from "../../@core/order/order.service";
import {filter, Observable} from "rxjs";
import {MockService} from "../../mock.service";
import {ageValidator} from "../../@core/validators/age.validator";
import {passportValidator} from "../../@core/validators/passport-validity.validator";
import {NotificationsService} from "../../@core/notifications/notifications.service";
import {MatDialog} from "@angular/material/dialog";
import {Router} from "@angular/router";
import {ConfirmDialogComponent} from "../../@shared/confirm-dialog/confirm-dialog.component";
import {IAirportSelector} from "../../@core/order/order.interface";

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss'],
})
export class OrderComponent implements OnInit {

  public airportSelectorData$: Observable<IAirportSelector[] | null> = this.orderService.airportsSelectorData$.pipe(filter(v => !!v));
  public nationality$: Observable<Nationality[] | null> = this.orderService.nationalities$.pipe(filter(v => !!v));
  public allowedDirections$: Observable<AllowedDirection[]> = this.mockService.getAllowedDirections();

  public documentType = PassengerDocumentType;
  public todayDate: Date = new Date();

  public orderForm: FormGroup = this.fb.group({
    airportFrom: [null, [Validators.required]],
    airportTo: [null, [Validators.required]],
    departureDate: ['', [Validators.required]],
    arrivalDate: ['', [Validators.required]],
    passengers: this.fb.array([])
  })

  public passengers = this.orderForm.get('passengers') as FormArray;

  constructor(private fb: NonNullableFormBuilder,
              private orderService: OrderService,
              private mockService: MockService,
              private ns: NotificationsService,
              private readonly dialog: MatDialog,
              private readonly router: Router) {
  }

  ngOnInit(): void {
    this.addPassenger();
    this.getFormFromStorage();
  }

  public changeAirportFrom(airport: Airport | null): void {
    this.orderForm.patchValue({airportFrom: airport})
  }

  public changeAirportTo(airport: Airport | null): void {
    this.orderForm.patchValue({airportTo: airport})
  }

  public addPassenger(): void {
    this.passengers.push(this.fb.group({
      documentType: ['', [Validators.required]],
      nationalityId: ['', [Validators.required]],
      passportDateOfExpire: ['', [Validators.required]],
      passportNo: ['', [Validators.required]],
      birthdate: ['', [Validators.required, ageValidator(20)]],
      lName: ['', [Validators.required]],
      fName: ['', [Validators.required]],
      sex: ['', [Validators.required]],
    }));
  }

  public removePassenger(index: number): void {
    if (this.passengers.length > 1) {
      this.passengers.removeAt(index);
    }
  }

  public setDocumentExpiresValidator(type: number, i: number): void {
    if (type !== 1) {
      return
    }
    this.passengers.at(i).get('passportDateOfExpire')?.setValidators(passportValidator(6))
    this.passengers.at(i).get('passportDateOfExpire')?.updateValueAndValidity();
  }

  public sendForm(): void {
    if (this.orderForm.invalid) {
      this.ns.notification('Необходимо заполнить все поля!', 3000, 'error')
      return
    }
    this.ns.notification('Форма сохранена', 1000, 'success');
    this.orderService.addOrderTicketForm(this.orderForm.value);
    this.router.navigate(['/result'])
  }

  public resetForm(): void {
    (this.orderForm.get('passengers') as FormArray).clear();
    this.orderForm.reset();
    this.orderService.clearLocalStorage();
    this.addPassenger();
  }

  public getFormFromStorage(): void {
    const order: Order = this.orderService.getOrderTicketForm();
    if (!order) {
      return;
    }
    this.dialog.open(ConfirmDialogComponent,
      {panelClass: 'dialog-panel'})
      .afterClosed()
      .subscribe((answer: boolean) => {
          if (answer) {
            (this.orderForm.get('passengers') as FormArray).clear();
            order.passengers.forEach(() => this.addPassenger());
            this.orderForm.patchValue(order);
          } else {
            this.resetForm();
          }
        }
      );
  }
}
