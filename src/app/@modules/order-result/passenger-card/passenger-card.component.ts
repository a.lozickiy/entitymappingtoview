import {Component, Input} from '@angular/core';
import {Nationality, Passenger, PassengerDocumentType} from "../../../mock.model";

@Component({
  selector: 'app-passenger-card',
  templateUrl: './passenger-card.component.html',
  styleUrls: ['./passenger-card.component.scss']
})
export class PassengerCardComponent {

  @Input() public i: number = 0;
  @Input() public passenger!: Passenger;
  @Input() public nationalities!: Nationality[];
  public passengersDocumentType = PassengerDocumentType;

  public getNationality(id: number): string {
    const nationality = this.nationalities?.find((item: Nationality) => item.id === id)
    return nationality ? nationality.name : '';
  }

}
