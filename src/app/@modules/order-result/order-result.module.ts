import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from "@angular/router";
import {orderResultRoutes} from "./order-result.routing";
import {OrderResultComponent} from "./order-result.component";
import {SharedModule} from "../../@shared/shared.module";
import { PassengerCardComponent } from './passenger-card/passenger-card.component';


@NgModule({
  declarations: [
    OrderResultComponent,
    PassengerCardComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(orderResultRoutes)
  ]
})
export class OrderResultModule {
}
