import {Component, OnDestroy} from '@angular/core';
import {OrderService} from "../../@core/order/order.service";
import {City, Country, Nationality, Order} from "../../mock.model";
import {combineLatest, filter, Subject, takeUntil} from "rxjs";

@Component({
  selector: 'app-order-result',
  templateUrl: './order-result.component.html',
  styleUrls: ['./order-result.component.scss']
})
export class OrderResultComponent implements OnDestroy {

  public orderTickets!: Order;
  public directionFrom: string[] = [];
  public directionTo: string[] = [];
  public nationalities: Nationality[] = [];
  private destroyed$: Subject<void> = new Subject<void>();

  constructor(private orderService: OrderService) {
    this.subscribeEmitters();
  }

  ngOnDestroy() {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  public buildFullRoute(order: Order, cities: City[], countries: Country[], direction: keyof Order): string[] {
    // @ts-ignore
    const city = cities.find((city: City) => order[direction].cityId === city.id);
    const country = countries.find((country: Country) => city?.countryId === country.id);
    // @ts-ignore
    return [country?.name, city?.name, order[direction].name]
  }

  private subscribeEmitters(): void {
    combineLatest([
      this.orderService.orderTicketForm$,
      this.orderService.cities$,
      this.orderService.countries$
    ])
      .pipe(
        takeUntil(this.destroyed$),
        filter(([order, cities, countries]) => !!order && !!cities && !!countries)
      )
      .subscribe(([order, cities, countries]) => {
        if (order) {
          this.directionFrom = this.buildFullRoute(order, cities, countries, 'airportFrom');
          this.directionTo = this.buildFullRoute(order, cities, countries, 'airportTo');
          this.orderTickets = order;
        }
      });

    this.orderService.nationalities$
      .pipe(
        takeUntil(this.destroyed$),
        filter((v) => !!v),
      )
      .subscribe((nationalities: Nationality[] | null) => {
          if (nationalities) {
            this.nationalities = nationalities;
          }
        }
      )
  }

}
