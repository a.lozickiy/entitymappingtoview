import {Routes} from "@angular/router";
import {OrderResultComponent} from "./order-result.component";

export const orderResultRoutes: Routes = [
  {
    path: '',
    component: OrderResultComponent
  },
];
