import {AbstractControl, ValidationErrors, ValidatorFn} from "@angular/forms";

export function ageValidator(age: number): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const twentyYearOld = new Date(new Date().setFullYear(new Date().getFullYear() - age));
    return twentyYearOld > new Date(control.value) ? null : {errorAge: true};
  };
}
