import {AbstractControl, ValidationErrors, ValidatorFn} from "@angular/forms";

export function passportValidator(month: number): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const validDate = new Date(new Date().setMonth(new Date().getMonth() + month));
    return validDate < control.value ? null : {errorExpire: true};
  };
}
