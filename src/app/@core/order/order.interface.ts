export interface IAirportSelector {
  id: number;
  name: string;
  systemName: string;
  defaultCurrencyId: number;
  isDeleted: boolean;
  children: City[];
}


export interface City {
  id: number;
  name: string;
  countryId: number;
  countryName?: string;
  iata?: string;
  systemName: string;
  isDeleted: boolean;
  children: Airport[];
}

export interface Airport {
  id: number;
  name: string;
  cityId: number;
  iata: string;
  parentId?: number;
  isVirtual: boolean;
  icao: string;
  altitude?: number;
  latitude?: number;
  longitude?: number;
  timezone?: number;
  systemName: string;
  isDeleted: boolean;
}
