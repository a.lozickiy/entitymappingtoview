import {Inject, Injectable} from '@angular/core';
import {MockService} from "../../mock.service";
import {BehaviorSubject, combineLatest, map, Observable, tap} from "rxjs";
import {Airport, City, Country, Nationality, Order} from "../../mock.model";
import {IAirportSelector} from "./order.interface";
import {LOCAL_STORAGE, StorageService} from "ngx-webstorage-service";

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  private airportsSelectorData: BehaviorSubject<IAirportSelector[] | null> = new BehaviorSubject<IAirportSelector[] | null>([]);
  public airportsSelectorData$: Observable<IAirportSelector[] | null> = this.airportsSelectorData.asObservable();

  private cities: BehaviorSubject<City[]> = new BehaviorSubject<City[]>([]);
  public cities$: Observable<City[]> = this.cities.asObservable();

  private countries: BehaviorSubject<Country[]> = new BehaviorSubject<Country[]>([]);
  public countries$: Observable<Country[]> = this.countries.asObservable();

  private nationalities: BehaviorSubject<Nationality[] | null> = new BehaviorSubject<Nationality[] | null>(null);
  public nationalities$: Observable<Nationality[] | null> = this.nationalities.asObservable();

  private orderTicketForm: BehaviorSubject<Order | null> = new BehaviorSubject<Order | null>(null);
  public orderTicketForm$: Observable<Order | null> = this.orderTicketForm.asObservable();


  constructor(private mockService: MockService,
              @Inject(LOCAL_STORAGE)
              private storage: StorageService) {
    this.subscribeEmitters();
  }

  public addOrderTicketForm(form: Order): void {
    this.orderTicketForm.next(form);
    this.storage.set('order', form);
  }

  public getOrderTicketForm(): Order {
    return this.storage.get('order');
  }

  public clearLocalStorage(): void {
    this.storage.clear()
  }

  private subscribeEmitters(): void {
    combineLatest([
      this.mockService.getCountries(),
      this.mockService.getCities(),
      this.mockService.getAirports()])
      .pipe(
        tap(([countries, cities, _]) => {
          this.countries.next(countries);
          this.cities.next(cities);
        }),
        map(([countries, cities, airports]) => {
          const cityWithAirports = cities.reduce((acc: any, city: City) => {
            const airportArray = airports.filter((airport: Airport) => city.id === airport.cityId)
            return airportArray.length > 0 ? [...acc, {...city, children: airportArray}] : acc
          }, [])

          return countries.reduce((acc: any, country: Country) => {
            const countryWithAirports = cityWithAirports.filter((city: City) => country.id === city.countryId)
            return countryWithAirports.length > 0 ? [...acc, {...country, children: countryWithAirports}] : acc
          }, [])

        })
      )
      .subscribe((result: IAirportSelector[]) => {
        this.airportsSelectorData.next(result);
      })

    this.mockService.getNationalities().subscribe((nationalities: Nationality[]) => this.nationalities.next(nationalities))
  }
}
