import {HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppComponent} from './app.component';
import {SharedModule} from "./@shared/shared.module";
import {OrderComponent} from './components/order/order.component';
import {AppRoutingModule} from "./app-routing.module";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {ReactiveFormsModule} from "@angular/forms";
import {AirportSelectorComponent} from './components/order/airport-selector/airport-selector.component';
import {MatNativeDateModule} from "@angular/material/core";
import {ConfirmDialogComponent} from "./@shared/confirm-dialog/confirm-dialog.component";


@NgModule({
  declarations: [
    AppComponent,
    OrderComponent,
    AirportSelectorComponent,
    ConfirmDialogComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    MatNativeDateModule,
    SharedModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {
}
