import {Routes} from "@angular/router";
import {OrderComponent} from "./components/order/order.component";

export const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'order'
  },
  {
    path: 'order',
    component: OrderComponent
  },
  {
    path: 'result',
    loadChildren: () => import('./@modules/order-result/order-result.module').then(m => m.OrderResultModule),
  },
  {
    path: '**',
    redirectTo: 'order'
  }
];
